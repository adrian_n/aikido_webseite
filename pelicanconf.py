#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Adrian Neumann'
SITENAME = u'Takemusu Aikido Berlin'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = u'de'
DEFAULT_DATE_FORMAT = ('%Y-%m-%d')
THEME = "./theme"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
MENUITEMS = (
	('Über Aikido', '/pages/takemusu-aikido.html'),
	('Trainingszeiten', '/pages/trainingszeiten-und-dojo.html'),
	('Seminare','/category/seminar.html'),
	('Prüfungsprogramme', '/pages/prufungsprogramme.html'),
	('Galerie','/pages/galerie.html'),
	('Links','/pages/links.html'))
STATIC_PATHS = ['exams']

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# # Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

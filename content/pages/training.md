Title: Trainingszeiten und Dojo

Trainiert wird

* Freitags 20:30-22:00
* Samstags 11:00-12:30

In der [Sportschule Rahn, Nicolaistraße 13 − 15, 12247 Berlin](http://www.openstreetmap.org/#map=18/52.44419/13.33460). Jeder ist willkommen, eine vorherige Anmeldung ist nicht nötig. 

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=13.332847952842712%2C52.44317702251511%2C13.33716630935669%2C52.44524026213023&amp;layer=mapnik&amp;marker=52.44420701950493%2C13.335009813308716" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/?mlat=52.44421&amp;mlon=13.33501#map=18/52.44421/13.33501">View Larger Map</a></small>
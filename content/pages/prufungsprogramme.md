Title: Prüfungsprogramme

Die Schülergrade (Kyu) zählt man in Japan traditionell absteigend, die Meistergrade (Dan) aufsteigend. Der 1. Kyu ist also der höchste Schülergrad, der 1. Dan hingegen der niedrigste Meistergrad.

Kinder
======

* [9. Kyu](../exams/Kinder987.pdf)
* [8. Kyu](../exams/Kinder987.pdf)
* [7. Kyu](../exams/Kinder987.pdf)
* [6. Kyu](../exams/Kinder654.pdf)
* [5. Kyu](../exams/Kinder654.pdf)
* [4. Kyu](../exams/Kinder654.pdf)

Erwachsene
==========

Schülergrade
------------

* [5. Kyu](../exams/Erwachsene54.pdf)
* [4. Kyu](../exams/Erwachsene54.pdf)
* [3. Kyu](../exams/Erwachsene32.pdf)
* [2. Kyu](../exams/Erwachsene32.pdf)
* [1. Kyu](../exams/Erwachsene1.pdf)

Meistergrade
------------

* [1. Dan](../exams/Shodan.pdf)
* [2. Dan](../exams/Nidan.pdf)
* [3. Dan](../exams/Sandan.pdf)
* [4. Dan](../exams/Yondan.pdf)
